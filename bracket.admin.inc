<?php

/**
 * @file
 * Bracket settings.
 */

/**
 * Page callback: Form constructor for the bracket administration form.
 *
 * @see bracket_settings_validate()
 * @see bracket_settings_submit()
 * @ingroup forms
 */
function bracket_settings() {

  $form = array();

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bracket Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );

  $form['general']['display'] = array(
    '#type' => 'select',
    '#title' => t('Default Display'),
    '#options' => bracket_display_options(),
    '#default_value' => variable_get('bracket_default_display', 'Image'),
    '#description' => t('Select the default display option for brackets. Use the button below to update all bracket display options to the current default display option - set the desired option first.')
  );

  $form['general']['update_bracket_display'] = array(
    '#type' => 'submit',
    '#value' => t('Update All Bracket Displays'),
  );

  $form['general']['upload'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow Image Uploads'),
    '#default_value' => variable_get('bracket_image_upload', FALSE),
    '#description' => t('Allow the user to upload images with brackets.')
  );

  $form['general']['upload_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Upload Path'),
    '#default_value' => variable_get('bracket_image_upload_path', ''),
    '#description' => t('This is the path where bracket images will be uploaded - if not specified, the default file path will be used.
                         This path must be writable by the user uploading the image.'),
    '#required' => FALSE
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration Settings'),
  );

  return $form;
}

/**
 * Validate bracket_settings form submissions.
 *
 * Checks if the upload path is writable.
 */
function bracket_settings_validate($form_id, &$form_state) {

  $upload = $form_state['values']['upload'];
  if ($upload) {
    $dir = $form_state['values']['upload_path'];
    if ($dir != '') {
      // must be writable
      $is_writable = file_prepare_directory($dir);
      if (!$is_writable) {
        form_set_error('upload_path', 'Upload path must be writeable');
      }
    }
  }
}

/**
 * Process bracket_settings form submissions.
 *
 * Updates all bracket displays if desired and sets variables.
 */
function bracket_settings_submit($form_id, &$form_state) {

  if ($form_state['values']['op'] == $form_state['values']['update_bracket_display']) {
    update_all_bracket_displays();
    $msg = 'Bracket display options were updated.';
    watchdog('bracket', $msg);
    drupal_set_message($msg, 'info');
    return;
  }

  variable_set('bracket_default_display', $form_state['values']['display']);
  variable_set('bracket_image_upload', $form_state['values']['upload']);
  variable_set('bracket_image_upload_path', $form_state['values']['upload_path']);

  drupal_set_message(t('Configuration settings were saved.'));
}

/**
 * Sets the display attribute on all brackets to bracket_default_display.
 */
function update_all_bracket_displays() {

  $option = variable_get('bracket_default_display', 'Image');
  $result = db_query("SELECT n.nid, n.title FROM {node} n WHERE n.type = 'bracket'");
  foreach ($result as $record) {
    $node = node_load($record->nid);
    $node->options['display'] = $option;
    node_save($node);
    watchdog('bracket', 'Updated bracket - %title (:nid) - to display option: :option', array("%title" => $row->title, ":nid" => $row->nid, ":option" => $option));
  }
}
